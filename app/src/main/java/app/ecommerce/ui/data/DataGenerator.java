package app.ecommerce.ui.data;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.DayPlan;
import app.ecommerce.ui.model.DirectionPlan;
import app.ecommerce.ui.model.ExampleProduct;

@SuppressWarnings("ResourceType")
public class DataGenerator {

    /**
     * Generate dummy data shopping product
     *
     * @param ctx android context
     * @return list of object
     */
    public static List<ExampleProduct> getProducts(Context ctx) {
        List<ExampleProduct> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.product_image);
        String title_arr[] = ctx.getResources().getStringArray(R.array.product_title);
        String price_arr[] = ctx.getResources().getStringArray(R.array.product_price);
        for (int i = 0; i < drw_arr.length(); i++) {
            ExampleProduct obj = new ExampleProduct();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.title = title_arr[i];
            obj.price = price_arr[i];
            obj.imageDrw = ctx.getResources().getDrawable(obj.image);
            items.add(obj);
        }
        return items;
    }

    public static List<DayPlan> getPlans(Context ctx) {
        String[] title = {
                "Three Days in Paris",
                "Main Sight",
                "My Weekend",
                "Last Year Planing"
        };

        String[] sight = {
                "3",
                "3",
                "3",
                "3"
        };

        int[] imglist = {
                R.drawable.eiffel,
                R.drawable.candi_kalasan_1,
                R.drawable.candi_kalasan_2,
                R.drawable.louvre,
        };

        List<DirectionPlan> dirPlanList = new ArrayList<>();
        dirPlanList.add(new DirectionPlan("Eiffel Tower", "0", "13"));
        dirPlanList.add(new DirectionPlan("Candi Kalasan", "3", "20"));
        dirPlanList.add(new DirectionPlan("Louvre", "9", "5"));

        List<DayPlan> dayPlanList = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            dayPlanList.add(new DayPlan(
                    title[i], sight[i],
                    imglist, dirPlanList
            ));
        }

        return dayPlanList;
    }
}
