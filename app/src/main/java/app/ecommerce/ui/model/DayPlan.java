package app.ecommerce.ui.model;

import android.graphics.drawable.Drawable;

import java.util.List;

public class DayPlan {
    public String title, sight;
    public int[] imgList;
    public List<DirectionPlan> directionList;

    public DayPlan(String title, String sight,
                   int[] imgList, List<DirectionPlan> directionList) {
        this.title = title;
        this.sight = sight + " sights";
        this.imgList = imgList;
        this.directionList = directionList;
    }

}
