package app.ecommerce.ui.model;

public class DirectionPlan {
    public String place, time, visitorWait, timeNumber;

    public DirectionPlan(String place, String time, String visitorWait) {
        this.place = place;
        this.time = time + " min by foot";
        this.timeNumber = time;
        this.visitorWait = "Visitors typically wait " + visitorWait + " min";
    }
}
