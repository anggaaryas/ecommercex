package app.ecommerce.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import app.ecommerce.ui.R;

public class DayPlansImgAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int[] imgs;

    public DayPlansImgAdapter(int[] imgs) {
        this.imgs = imgs;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.day_plans_img_item, parent, false);
        return new DayPlansImgAdapter.OriginalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof OriginalViewHolder){
            OriginalViewHolder v = (OriginalViewHolder) holder;
            v.ivImg.setImageResource(imgs[position]);
        }
    }

    @Override
    public int getItemCount() {
        return imgs.length;
    }

    class OriginalViewHolder extends RecyclerView.ViewHolder{
        ImageView ivImg;

        OriginalViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImg = itemView.findViewById(R.id.ivImg);
        }
    }
}
