package app.ecommerce.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.data.DataGenerator;
import app.ecommerce.ui.model.DayPlan;
import app.ecommerce.ui.model.DirectionPlan;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class DayPlansAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DayPlan> dayPlanList;
    public OnItemClickListener listener = new OnItemClickListener() {
        @Override
        public void onItemClick(View view, DayPlan obj, int position) {
            // Default Listener. No action
            Log.i(TAG, "Listener belum ditambah");
        }
    };

    public DayPlansAdapter(List<DayPlan> dayPlanList){
        this.dayPlanList = dayPlanList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.day_plans_item, parent, false);
        return new OriginalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof OriginalViewHolder){
            OriginalViewHolder v = (OriginalViewHolder) holder;
            final DayPlan item = dayPlanList.get(position);

            v.tvTitle.setText(item.title);
            v.tvSight.setText(item.sight);

            v.setDirectionList(item.directionList);
            v.setImgList(item.imgList);

            v.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(v, item, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dayPlanList.size();
    }

    class OriginalViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvSight;
        RecyclerView rcImg, rcDirection;
        ImageView ivDirIco;

        OriginalViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tvTitle);
            tvSight = v.findViewById(R.id.tvInfoSight);
            rcImg = v.findViewById(R.id.rcImg);
            rcDirection = v.findViewById(R.id.rcDirection);
            ivDirIco = v.findViewById(R.id.ivDirectionIco);

            setDirIcoListener();
        }

        void setDirectionList(List<DirectionPlan> list){
            rcDirection.setLayoutManager(new LinearLayoutManager((itemView.getContext())));
            rcDirection.setAdapter(new DayPlansDirectionAdapter(list));
        }

        void setImgList(int[] imgs){
            rcImg.setLayoutManager(new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false));
            rcImg.setAdapter(new DayPlansImgAdapter(imgs));
        }

        private void setDirIcoListener(){
            ivDirIco.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setRotation((v.getRotation() + 180) % 360);
                    v.invalidate();
                    if(rcDirection.getVisibility() == View.VISIBLE){
                        rcDirection.setVisibility(View.GONE);
                    } else {
                        rcDirection.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, DayPlan obj, int position);
    }
}
