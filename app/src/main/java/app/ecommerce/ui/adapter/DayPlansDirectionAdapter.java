package app.ecommerce.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.DirectionPlan;

public class DayPlansDirectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DirectionPlan> list;

    public DayPlansDirectionAdapter(List<DirectionPlan> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.day_plans_direction_item, parent, false);
        return new DayPlansDirectionAdapter.OriginalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof OriginalViewHolder){
            OriginalViewHolder v = (OriginalViewHolder) holder;
            DirectionPlan item = list.get(position);
            String pos = "" + (position + 1);
            v.tvNumb.setText(pos);
            v.tvDir.setText(item.place);
            v.tvTime.setText(item.time);
            v.tvVisitorTime.setText(item.visitorWait);

            if(item.timeNumber.equals("0")) v.llWalk.setVisibility(View.GONE);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class OriginalViewHolder extends RecyclerView.ViewHolder{
        TextView tvVisitorTime, tvTime, tvDir, tvNumb;
        LinearLayout llWalk;

        OriginalViewHolder(@NonNull View itemView) {
            super(itemView);

            tvVisitorTime = itemView.findViewById(R.id.tvVisitorTime);
            tvTime = itemView.findViewById(R.id.tvInfoTime);
            tvDir = itemView.findViewById(R.id.tvDir);
            tvNumb = itemView.findViewById(R.id.tvNumb);
            llWalk = itemView.findViewById(R.id.llWalk);

        }
    }


}
