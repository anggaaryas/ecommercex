package app.ecommerce.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.DayPlansAdapter;
import app.ecommerce.ui.data.DataGenerator;
import app.ecommerce.ui.model.DayPlan;
import app.ecommerce.ui.utils.Tools;

public class ActivityDayPlans extends AppCompatActivity {
    private RecyclerView rcMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);

        initToolbar();
        initComponent();

    }

    private void initComponent(){
        DayPlansAdapter adapter = new DayPlansAdapter(DataGenerator.getPlans(getApplicationContext()));
        adapter.listener = new DayPlansAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, DayPlan obj, int position) {
                Toast.makeText(getApplicationContext(), "Clicked! position " + position, Toast.LENGTH_SHORT).show();
            }
        };

        rcMain = findViewById(R.id.rcMain);
        rcMain.setLayoutManager(new LinearLayoutManager(this));
        rcMain.setAdapter(adapter);
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        //change menu button color
        Tools.changeNavigationIconColor(toolbar, getResources().getColor(R.color.grey_60));
        //change overflow menu button color
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.grey_60));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day Plans");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.example_menu, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
